import numpy as np

K = input("Enter matrix size: ")
K *= 2

matrix = np.random.rand(K, K)
print(matrix)

def rotate(matrix):
    newmatrix = [[0 for x in range(K)] for y in range(K)] 
    for i in range(K):
        for j in range(K):
            if (i < (K / 2) and j < (K / 2)):
                newmatrix[i][j + K / 2] = matrix[i][j]
            if (i < (K / 2) and j > (K / 2 - 1)):
                newmatrix[i + K / 2][j] = matrix[i][j]
            if (i > (K / 2 - 1) and j > ( K / 2 - 1)):
                newmatrix[i][j - K / 2]= matrix[i][j]
            if (i > (K / 2 - 1) and j < (K / 2)):
                newmatrix[i - K / 2][j] = matrix[i][j]
            
            
    return newmatrix        
            
def printMat(matrix, file):
    for row in matrix:
        file.write(row)

rotated = rotate(matrix)
# printMat(rotated)
def writeMatrix(matrix, rotatedMatrix):
    np.savetxt('matrix', matrix)
    np.savetxt('rotated_matrix', rotatedMatrix)

writeMatrix(matrix, rotated)    